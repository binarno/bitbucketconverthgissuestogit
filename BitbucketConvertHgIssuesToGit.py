from typing import Dict, List
import getopt
import sys



def readMappingFile(fileName: str) -> Dict[str, str]:

    return_dict = {}

    with open(fileName, 'r') as file:
        while True:
            line: str = file.readline()

            if len(line) == 0:
                break

            if line[0] != ':':
                continue

            elements: List[str] = line.strip(' \r\n\t:').split(' ')
            if len(elements) != 2:
                continue

            return_dict[elements[0]] = elements[1]

    return return_dict


def main():

    try:
        options = ["help", "repositoryFolder=", "inputJsonFile=", "outputJsonFile="]
        opts, args = getopt.getopt(sys.argv[1:], "", options)

    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err))
        usage()
        sys.exit(2)

    repositoryFolder = ""
    inputJsonFile = ""
    outputJsonFile = ""
    
    for option, value in opts:
        if option == '--help':
            usage()
            sys.exit(0)

        if option == '--repositoryFolder':
            repositoryFolder = value

        if option == '--inputJsonFile':
            inputJsonFile = value

        if option == '--outputJsonFile':
            outputJsonFile = value

    if len(repositoryFolder) == 0 or len(inputJsonFile) == 0 or len(outputJsonFile) == 0:
        usage()
        sys.exit(2)

    hgHashToRevision: dict[str, str] = readMappingFile('./.git/hg2git-mapping')
    revisionToGitHash: dict[str, str] = readMappingFile('./.git/hg2git-marks')

    with open(inputJsonFile, 'r') as inputJson:

        jsonContent = inputJson.read()

        for hgHash, hgRevision in hgHashToRevision.items():

            if (str(int(hgRevision) + 1)) in revisionToGitHash:
                print("Replacing " + hgHash + " with " + revisionToGitHash.get(str(int(hgRevision) + 1)))

                for length in range(len(hgHash), 8, -1):

                    newJsonContent = jsonContent.replace(hgHash[0:length], revisionToGitHash.get(str(int(hgRevision) + 1)))
                    jsonContent = newJsonContent

        with open(outputJsonFile, 'w') as outputJson:
            outputJson.write(jsonContent)


def usage():
    print ("Usage: python BitbucketConvertHgIssuesToGit.py --repositoryFolder=path_to_git_repo --inputJsonFile=path_to_issues_json_file --outputJsonFile=path_to_new_issues_json_file")

if __name__== "__main__":
    main()
