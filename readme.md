# Bitbucket: convert Hg issues to Git

## Introduction

Unfortunately, Bitbucket has deprecated Mercurial (my preferred version control system) in favor of Git (I hate it).

Anyway, converting a Mercurial repo to Git is straightforward, but the issues that contain links to your Hg commits will fail to link to the same commits in Git.

This simple script converts the commit hashes in the issues from Mercurial to Git, so the commits referenced in your issues will show up from the Git repo.

## Usage

### Step 1: Convert your Hg repo to a Git one

- make a copy of your Hg folder
- Create an empty folder for your git repo
- cd into the new folder
- `git init`
- `hg-fast-export -r path_to_copy_of_the_hg_repo`
- if the export complains about a detached head, strip that revision from your Hg copy with `hg strip revision_number` and hg-fast-export again

### Step 2: Create a new repo on Bitbucket

- On Bitbucket, create a new Git repository
- `git remote add origin bitbucket_new_repo_git_address`
- `git push --all`

### Step 3: Export the issues from the Bitbucket Hg repo

- On the Hg repo in Bitbucket, go on **Settings**, **Import & Export**, **Start Export**

### Step 4: Convert the hashes in the issues from Hg to Git

- Unzip the file created during the export phase
- run `python3 BitbucketConvertHgIssuesToGit.py --repositoryFolder=path_to_local_git_folder --inputJsonFile=path_to_unzipped_export/db-1.0.json --outputJsonFile=path_to_unzipped_export/outdb-1.0.json`
- run `python3 BitbucketConvertHgIssuesToGit.py --repositoryFolder=path_to_local_git_folder --inputJsonFile=path_to_unzipped_export/db-2.0.json --outputJsonFile=path_to_unzipped_export/outdb-2.0.json`
- delete db-1.0.json and db-2.0.json from path_to_unzipped_export
- in the unzipped folder, rename outdb-1.0.json to db-1.0.json and outdb-2.0.json to db-2.0.json
- zip the files in the unzipped folder

### Step 5: Import the converted issues into the Bitbucket Git repo

- On Bitbucket, Select the new git repo
- go on **Settings**, **Import & Export**, **Browse**
- select the newly zipped file
- click Import

## How does it work?

hg-fast-export creates few extra files in the .git folder of the new git repo.

Two files in particular contain the mapping from hg hashes to revision numbers (hg2git-mapping) and from revision number to git hashes (hg2git-marks).

The script looks for all the Hg hashes in the exported issues (also shortened hashes) and replace them with the git hashes.




